import os
import sys
import json
import time
import requests
import websocket
from keep_alive import keep_alive

status = "dnd"  # Do Not Disturb (DND) olarak ayarla

custom_status = os.getenv("CUSTOM_STATUS", "")  # Özel durumu al

def get_token():
    usertoken = os.getenv("TOKEN")
    if not usertoken:
        print("[ERROR] Please add a token inside Secrets.")
        sys.exit()
    return usertoken

def refresh_token(headers):
    try:
        validate = requests.get("https://canary.discordapp.com/api/v9/users/@me", headers=headers)
        if validate.status_code != 200:
            print("[ERROR] Your token might be invalid. Please check it again.")
            sys.exit()
    except requests.exceptions.RequestException as e:
        print("[ERROR] An error occurred during token refresh:", e)
        sys.exit()

def get_user_info(headers):
    try:
        userinfo = requests.get("https://canary.discordapp.com/api/v9/users/@me", headers=headers).json()
        username = userinfo["username"]
        discriminator = userinfo["discriminator"]
        userid = userinfo["id"]
        return username, discriminator, userid
    except (KeyError, json.JSONDecodeError) as e:
        print("[ERROR] Failed to retrieve user information:", e)
        sys.exit()

def onliner(token, status):
    try:
        ws = websocket.WebSocket()
        ws.connect("wss://gateway.discord.gg/?v=9&encoding=json")
        start = json.loads(ws.recv())
        heartbeat = start["d"]["heartbeat_interval"]
        auth = {
            "op": 2,
            "d": {
                "token": token,
                "properties": {
                    "$os": "Windows 10",
                    "$browser": "Google Chrome",
                    "$device": "Windows",
                },
                "presence": {"status": status, "afk": False},
            },
            "s": None,
            "t": None,
        }
        ws.send(json.dumps(auth))
        cstatus = {
            "op": 3,
            "d": {
                "since": 0,
                "activities": [
                    {
                        "type": 4,
                        "state": custom_status,
                        "name": "Custom Status",
                        "id": "custom",
                        # Uncomment the below lines if you want an emoji in the status
                        # "emoji": {
                        #     "name": "emoji name",
                        #     "id": "emoji id",
                        #     "animated": False,
                        # },
                    }
                ],
                "status": status,
                "afk": False,
            },
        }
        ws.send(json.dumps(cstatus))
        online = {"op": 1, "d": "None"}
        time.sleep(heartbeat / 1000)
        ws.send(json.dumps(online))
    except (websocket.WebSocketException, json.JSONDecodeError) as e:
        print("[ERROR] An error occurred during WebSocket communication:", e)

def run_onliner():
    os.system("clear")
    
    usertoken = get_token()
    headers = {"Authorization": usertoken, "Content-Type": "application/json"}
    
    while True:
        try:
            refresh_token(headers)
            username, discriminator, userid = get_user_info(headers)

            print(f"Logged in as {username}#{discriminator} ({userid}).")

            onliner(usertoken, status)
            time.sleep(30)  # Add delay
        except KeyboardInterrupt:
            break
        except Exception as e:
            print("[ERROR] An unexpected error occurred:", e)
            time.sleep(60)  # Add delay before retrying

keep_alive()
run_onliner()